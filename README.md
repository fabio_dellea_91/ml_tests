# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contains some machine learning tests in Java.

Currently the only project (ml_test) is about text classification algorithms. [k-nearest neighbor applied to bag of words model, with inverse terms frequency ranking algorithm]

### How do I get set up? ###

Clone the repository in Eclipse and import the project.
Go to folder resources. Unzip file news_aggregator.zip in same folder.

Go to MLPipeline.java file. Set/check effective input data file path (row 20/21).
Set title to classify at row 18. 4 different category in dataset: business, health, technology, entertainment.

### Contribution guidelines ###

News dataset (news_aggregator.csv) from:
Lichman, M. (2013). UCI Machine Learning Repository [http://archive.ics.uci.edu/ml]. Irvine, CA: University of California, School of Information and Computer Science.
