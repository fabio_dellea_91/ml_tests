package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import enums.UCIArticlesCategoryEnum;
import objects.TextVector;
import objects.UCIArticles;

public class DataHandler {
	
	public static List<UCIArticles> getDataFromCSV(String filename, String delimiter){
        List<UCIArticles> result = new ArrayList<UCIArticles>();
        
		String line = "";
        
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] parts = line.split(delimiter);

            	UCIArticles elem = new UCIArticles();
            	
            	try{
            		//elem.setId(Double.parseDouble(parts[0]));
            		if(parts.length>5){
	            		elem.setTitle(parts[1].toLowerCase());
	            		elem.setCategory(UCIArticlesCategoryEnum.findByCode(parts[4]));
	            		result.add(elem);
            		}
            		
            	}
            	catch(Exception e){
            		e.printStackTrace();
            	}
                
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
		return result;
	}
	
	public static List<String> getStopwordsFromFile(String filename){
		List<String> result = new ArrayList<String>();
        
		String line = "";
        
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {

            while ((line = br.readLine()) != null) {

               result.add(line.toLowerCase());
                
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        
		return result;
	}
	
	public static List<TextVector> convertAndCleanData(List<UCIArticles> articles, List<String> stopwords){
		List<TextVector> result = new ArrayList<TextVector>();
		
		for(UCIArticles article : articles){
			String[] words = article.getTitle().split(" ");
			
			TextVector vector = new TextVector();
			vector.setVector(new HashMap<String,Integer>());
			vector.setCategory(article.getCategory());
			
			for(String word : words){
				String lowercase = word.toLowerCase();
				
				if(stopwords.contains(lowercase))
					continue;
				
				if(vector.getVector().containsKey(lowercase)){
					vector.getVector().put(lowercase, vector.getVector().get(lowercase) + 1);
				}
				else{
					vector.getVector().put(lowercase, 1);
				}
			}

			result.add(vector);
		}
		
		return result;
	}
	
	public static void calculateSimilarity(String sentence, List<TextVector> dataset){
		String[] words = sentence.split(" ");
		Map<String,Integer> wordSentenceFrequencyMap = new HashMap<String,Integer>();
		Map<String,Integer> wordDocumentFrequencyMap = new HashMap<String,Integer>();
		
		for(String word : words){
			String lowercase = word.toLowerCase();
			
			if(wordSentenceFrequencyMap.containsKey(lowercase)){
				wordSentenceFrequencyMap.put(lowercase, wordSentenceFrequencyMap.get(lowercase) + 1);
			}
			else{
				wordSentenceFrequencyMap.put(lowercase, 1);
			}
		}
		
		for(String word : wordSentenceFrequencyMap.keySet()){
			Integer documentFrequency = calculateDocumentFrequency(word, dataset);
			wordDocumentFrequencyMap.put(word, documentFrequency);		
		}
		
		for(TextVector elem : dataset){
			BigDecimal rank = BigDecimal.ZERO;
			
			Set<String> intersection = elem.getVector().keySet();
			intersection.retainAll(wordSentenceFrequencyMap.keySet());
			
			for(String el : intersection){
				rank = rank.add(partialRankingBM25_TF(wordSentenceFrequencyMap.get(el), elem.getVector().get(el), wordDocumentFrequencyMap.get(el), dataset.size()));
			}
			
			elem.setRank(rank);
		}
	}
	
	private static Integer calculateDocumentFrequency(String keyword, List<TextVector> dataset){
		Integer documentFrequency = 0;
		
		for(TextVector elem : dataset){
			if(elem.getVector().containsKey(keyword)){
				documentFrequency++;
			}
		}
		
		return documentFrequency;
	}
	
	private static BigDecimal partialRankingBM25_TF(Integer wordSencenceCount, Integer wordDocumentCount, Integer documentFrequency, Integer datasetSize){
		Integer k = 3;
		
		if(wordDocumentCount == null)
			return BigDecimal.ZERO;
		
		return new BigDecimal(wordSencenceCount).
								multiply(
										new BigDecimal(k+1).multiply(
												new BigDecimal(wordDocumentCount)
										).divide(new BigDecimal(wordDocumentCount + k))
								).
								multiply(
										new BigDecimal(java.lang.Math.log((datasetSize+1)/documentFrequency))
								);
	}
}