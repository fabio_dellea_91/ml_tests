package core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import objects.TextVector;
import objects.UCIArticles;
import enums.UCIArticlesCategoryEnum;

public class MLPipeline {
	static final int k = 5;
	
	public static void main(String[] args) {
		//String testSentence = "Functioning Muscles Grown from Skin Cells"; // health
		String testSentence = "Toyota and Mazda Are Said to Pick Alabama for $1.6 Billion Plant"; // business
		
		List<UCIArticles> articles = DataHandler.getDataFromCSV("C:\\workspace\\ml_tests\\ml_tests\\ml_test\\resources\\news_aggregator.csv",",");
		List<String> stopwords = DataHandler.getStopwordsFromFile("C:\\workspace\\ml_tests\\ml_tests\\ml_test\\resources\\stopwords.txt");
		
		List<TextVector> datasetVectors =  DataHandler.convertAndCleanData(articles,stopwords);
		
		DataHandler.calculateSimilarity(testSentence, datasetVectors);
		
		List<TextVector> neighbors = new ArrayList<TextVector>();
		
		for(TextVector el : datasetVectors){
			if(neighbors.size() < k){
				neighbors.add(el);
			}
			else{
				TextVector farest = null;
				
				for(TextVector neighbor : neighbors){
					if(farest == null){
						farest = neighbor;
					}
					else{
						if(neighbor.getRank().compareTo(farest.getRank()) < 0){
							farest = neighbor;
						}
					}
				}
				
				if(el.getRank().compareTo(farest.getRank()) > 0){
					neighbors.remove(farest);
					neighbors.add(el);
				}
			}
		}
		
		UCIArticlesCategoryEnum[] categories = UCIArticlesCategoryEnum.values();
		Map<UCIArticlesCategoryEnum, Integer> results = new HashMap<UCIArticlesCategoryEnum, Integer>();		
		
		for (int i=0 ; i<categories.length ; i++) {
			results.put(categories[i], 0);
		}
		
		for(TextVector el : neighbors){
			results.put(el.getCategory(), results.get(el.getCategory()) + 1);
		}
		
		UCIArticlesCategoryEnum bestFit = null;
		for(UCIArticlesCategoryEnum category : results.keySet()){
			if(bestFit == null){
				bestFit = category;
			}
			else{
				if(results.get(category) > results.get(bestFit)){
					bestFit = category;
				}
			}
		}
		
		System.out.println("Article category is " + bestFit + " with " + (new BigDecimal(results.get(bestFit) / k).multiply(new BigDecimal(100))).intValue() +"% of probability");
	}

}
