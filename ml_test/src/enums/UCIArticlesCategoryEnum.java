package enums;

public enum UCIArticlesCategoryEnum {
	BUSINESS ("b"),
	TECHNOLOGY ("t"),
	ENTERTEINMENT ("e"),
	HEALTH ("m");
	
	private String category;
	
	private UCIArticlesCategoryEnum(String category){
		this.category=category;
	}
	
	public String getCategory() {
		return category;
	}
	
	public static UCIArticlesCategoryEnum findByCode(String scope) {
		UCIArticlesCategoryEnum[] textScopes = UCIArticlesCategoryEnum.values();
		for (int i=0 ; i<textScopes.length ; i++) {
			if (textScopes[i].getCategory().equals(scope)) {
				return textScopes[i];
			}
		}
		return null;
    }
}