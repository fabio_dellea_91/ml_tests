package objects;

import java.util.Date;

import enums.UCIArticlesCategoryEnum;

public class UCIArticles{
	private double id;
	private String title;
	private String url;
	private String publisher;
	private UCIArticlesCategoryEnum category;
	private String story;
	private String hostname;
	private Date timestamp;
	
	
	public double getId() {
		return id;
	}
	public void setId(double id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public UCIArticlesCategoryEnum getCategory() {
		return category;
	}
	public void setCategory(UCIArticlesCategoryEnum category) {
		this.category = category;
	}
	public String getStory() {
		return story;
	}
	public void setStory(String story) {
		this.story = story;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	@Override
	public String toString() {
		return "UCIArticles [id=" + id + ", title=" + title + ", url=" + url
				+ ", publisher=" + publisher + ", category=" + category
				+ ", story=" + story + ", hostname=" + hostname
				+ ", timestamp=" + timestamp + "]";
	}
}