package objects;

import java.math.BigDecimal;
import java.util.Map;

import enums.UCIArticlesCategoryEnum;

public class TextVector {
	private Map<String,Integer> vector;
	private UCIArticlesCategoryEnum category;
	private BigDecimal rank;
	
	public Map<String, Integer> getVector() {
		return vector;
	}
	public void setVector(Map<String, Integer> vector) {
		this.vector = vector;
	}
	public UCIArticlesCategoryEnum getCategory() {
		return category;
	}
	public void setCategory(UCIArticlesCategoryEnum category) {
		this.category = category;
	}
	public BigDecimal getRank() {
		return rank;
	}
	public void setRank(BigDecimal rank) {
		this.rank = rank;
	}
	
	
}